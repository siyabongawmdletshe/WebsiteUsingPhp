<?php
/**
 * User: masiya
 * 
 * This is the Items page
 */

include 'Connections.php';
session_start();

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>TechGurus</title>

    <!-- Bootstrap -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">
    <link href="../css/custom.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="../js/html5shiv.min.js"></script>
    <script src="../js/respond.min.js"></script>
    <![endif]-->
</head>

<style>
    body{
        padding-top: 40px;
    }

    .modal{
        padding-top: 20px;
    }


</style>

<body data-spy="scroll" data-target="#my-navbar">

</header>

<!--Navbar-->
   <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation" id="my-navbar">
      <div class="container">
       <div class="navbar-header">
            <a href="Home.php" ><img src="../Images/logo.png" id="logo" class="img-responsive"></a>
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#list-to-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
      </div>
          <div class="collapse navbar-collapse" id="list-to-collapse">

       <ul class="nav navbar-nav">
           <li><li>
           <li><a id="link1" style="color:white;" href="Home.php?#Computers" onmouseover="changeColor(this,'#9d9d9d')" onmouseout="changeColor(this,'white')">Computers</a><li>
            <li><a id="link2" style="color:white"  href="Home.php?#CableAndAdapters" onmouseover="changeColor(this,'#9d9d9d')" onmouseout="changeColor(this,'white')">Adapters & Cables</a><li>
           <li><a id="link3"style="color:white" href="Home.php?#Games" onmouseover="changeColor(this,'#9d9d9d')" onmouseout="changeColor(this,'white')">Gaming</a><li>
           <li><a id="link4"style="color:white" href="Home.php?#Networking" onmouseover="changeColor(this,'#9d9d9d')" onmouseout="changeColor(this,'white')">Networking</a><li>
           <li><a id="link5" style="color:white;" href="Home.php?#Softwares" onmouseover="changeColor(this,'#9d9d9d')" onmouseout="changeColor(this,'white')">Software</a><li>
           <li><a id="link6" style="color:white"  href="Home.php?#Speakers" onmouseover="changeColor(this,'#9d9d9d')" onmouseout="changeColor(this,'white')">Speakers</a><li>
           <li><a id="link7"style="color:white" href="Home.php?#Storages" onmouseover="changeColor(this,'#9d9d9d')" onmouseout="changeColor(this,'white')">Storage</a><li>
           <li><a id="link8"style="color:white" href="" onmouseover="changeColor(this,'#9d9d9d')" onmouseout="changeColor(this,'white')">Support</a><li>

       </ul>

      </div>
      </div>

   </nav>            <!--Navbar end-->
                <!--Header-->
    <div class="jumbotron text-center">
        <div class="container">
            <h1>TechGurus</h1>
             <h3> We assemble, retail and wholesale Information Technology products and services.</h3>
        </div>
    </div>
<!--header end-->
   </header>



<?php

if(isset($_GET['DeleteItemId'])){
    $Delesql = ("Delete from orders where orderID='$_GET[DeleteItemId]'");//query
    mysqli_query($con, $Delesql);
}

if(isset($_POST['UpdateQuantity'])){

    for($i=1; $i<=$_SESSION['rows']; $i++){
              $q=$_SESSION['orderIDS']['UpdateQuantity'.$i];
              $q2="UpdateQuantity".$i;
         $UpdateSql = ("Update orders set Quantity='$_POST[$q2]' where orderID='$q'");
          mysqli_query($con, $UpdateSql);


    }


}




if(isset($_SESSION['UserID']) && isset($_SESSION['ItemID']) && $_SESSION['Quantity'] ) {

$inner= ("SELECT orders.orderID, orders.Quantity, user.Fname, user.Lname, user.Email, item.ItemId,item.Price,item.Description 
FROM user
INNER JOIN orders
ON user.UserID=orders.UserId
INNER JOIN item
ON item.ItemId=orders.ItemId 
where user.UserID='$_SESSION[UserID]'
");

    $SqlResult2 = mysqli_query($con, $inner);
    $row_cnt = mysqli_num_rows($SqlResult2);



    if($row_cnt>0) {

        ?>


        <div class="container">

            <table class="table table-bordered">
                <caption>Shopping Cart
                <h5 style="float: right"><?php echo "Number of Items: ".$row_cnt?></h5>
                </caption>
                <thead>
                <tr>

                    <td>
                        Name

                    </td>

                    <td>
                        OrderID

                    </td>


                    <td>
                        Item ID

                    </td>


                    <td>
                        Description

                    </td>

                    <td>
                        Price

                    </td>

                    <td>
                        Quantity

                    </td>

                    <td>
                        Total Price

                    </td>

                    <td>
                        Action

                    </td>
                </tr>

                </thead>
                <tbody>
                <?php

                $SUBtotal = 0;
                $totalItems=0;
                $Count_R=0;
                while ($row = mysqli_fetch_array($SqlResult2, MYSQLI_BOTH)) {
                    $total = $row['Price'] * $row['Quantity'];
                    $SUBtotal += $total;
                    $totalItems+=$row['Quantity'];
                    $Count_R++;
                    $_SESSION['rows']=$Count_R;
                    $_SESSION['SubTotal']=$SUBtotal;
                    $_SESSION['TotalItems']=$totalItems;
                    $_SESSION["Email"]=$row['Email'];

                    ?>
                    <tr>

                        <td>
                            <?php echo $row['Fname'] . " " . $row['Lname'] ?>

                        </td>

                        <td>
                            <?php echo $row['orderID'] ?>

                        </td>

                        <td>
                            <?php echo $row['ItemId'] ?>

                        </td>


                        <td>
                            <?php echo $row['Description'] ?>

                        </td>

                        <td>
                            <?php echo "R" . $row['Price'] ?>

                        </td>
                        <td>

                            <form role="form" method="post" action="#">

							

                           <input type="number" min="1"  max="100" name="UpdateQuantity<?php echo $Count_R; $_SESSION['orderIDS']['UpdateQuantity'.$Count_R]=$row['orderID']?>" value="<?php echo $row['Quantity'] ?>">

                        </td>

                        <td>
                            <?php echo "R" . $total ?>

                        </td>

                        <td>
                            <a href="" type="button" class="btn btn-warning" data-toggle="modal" data-target="#modal-1">Delete</a>
                             <div class="modal fade" id="modal-1">
                                    <div class="modal-dialog modal-lg">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                <h3 class="modal-title">Confirm delete!</h3>
                                            </div>
                                            <div class="modal-body">
                                                Are you sure you want to delete this item from your shopping cart?
                                            </div>

                                            <div class="modal-footer">
                                                <a href="" class="btn btn-success" data-dismiss="modal">No</a>
                                                <a href="ItemCarts.php?DeleteItemId=<?php echo $row['orderID']; ?>" class="btn btn-danger">Yes</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                        </td>

                    </tr>


                    <?php

                }


                ?>
                </tbody>
            </table>

          <?php
            echo "<div class='row'><div class='col-md-6 text-right'><h2>SUBTOTAL:</h2> </div><div class='col-md-4 text-left'><h2><div><p>$SUBtotal</p></div></div></div>";
               ?>

            <div class="row">
                <div class="col-sm-4 text-center">

                    <button type="submit" name="UpdateQuantity" class="btn btn-primary text-center">UPDATE ORDER</button>
                   <!-- <a href="#?UpdateQnty=0" type="button" class="btn btn-primary text-center">UPDATE ORDER<a/>-->
                </div>

                </form> <!----FORM END---->


                <div class="col-sm-4 text-center">
                    <a href="Home.php" type="button" class="btn btn-primary text-center">KEEP SHOPPING<a/>
                </div>

                <div class="col-sm-4 text-center">   <!--CHECKOUT BUTTON--->

                    <a href="" type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal-2">CHECKOUT</a>
                        <div class="modal fade" id="modal-2">
                            <div class="modal-dialog modal-lg">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h3 class="modal-title">Receipt</h3>
                                    </div>

                                    <div class="modal-body"> <!--RECEIPT USING MODAL---->

                                      <?php

                                      $SqlResult3 = mysqli_query($con, $inner);
                                      $row_cnt = mysqli_num_rows($SqlResult3);

                                             echo "<div class='row'><div class='col-md-2 col-md-offset-5'>Name: </div><div class='col-md-2'>$_SESSION[FName]&nbsp;$_SESSION[LName]</div></div>";
                                             echo "<div class='row'><div class='col-md-2 col-md-offset-5'>Email: </div><div class='col-md-2'>$_SESSION[Email]</div></div>";
                                      echo "<hr>";
                                      $rowcount=0;

                                      while ($row2 = mysqli_fetch_array($SqlResult3, MYSQLI_BOTH)) {
                                            $rowcount++;
                                       ?>





                                                   <?php



                                                   echo "<div class='row'><div class='col-md-2 col-md-offset-5'>Order number: </div><div class='col-md-2'>$rowcount</div></div>";
                                                   echo "<div class='row'><div class='col-md-2 col-md-offset-5'>OrderId: </div><div class='col-md-2'>$row2[orderID] </div></div>";
                                                   echo "<div class='row'><div class='col-md-2 col-md-offset-5'>IItemId: </div><div class='col-md-2'>$row2[ItemId]</div></div>";
                                                   echo "<div class='row'><div class='col-md-2 col-md-offset-5'>Description: </div><div class='col-md-2'>$row2[Description] </div></div>";
                                                   echo "<div class='row'><div class='col-md-2 col-md-offset-5'>Quantity: </div><div class='col-md-2'>$row2[Quantity] </div></div>";
                                                   echo "<div class='row'><div class='col-md-2 col-md-offset-5'>Price: </div><div class='col-md-2'><div><p>$row2[Price] </p></div></div></div>";

                                   }

                                      ?>



                                                <?php
                                                $vat=$_SESSION['SubTotal']*1.14;
                                                $VP=(-1*($_SESSION['SubTotal']-$vat));
                                                echo "<div class='row'><div class='col-md-2 col-md-offset-5'>Items:</div><div class='col-md-2'>$_SESSION[TotalItems] </div></div>";
                                                echo "<div class='row'><div class='col-md-2 col-md-offset-5'>SubTotal(Ex VAT): </div><div class='col-md-2'><div><p>$_SESSION[SubTotal] </p></div></div></div>";
                                                echo "<div class='row'><div class='col-md-2 col-md-offset-5'>VAT Portion(14%): </div><div class='col-md-2'><div><p>$VP</p></div></div></div>";
                                                echo "<div class='row'><div class='col-md-2 col-md-offset-5'>Total(Inc VAT): </div><div class='col-md-2'><div><p>$vat</p></div></div></div>";


                                      ?>


                                    </div>

                                    <div class="modal-footer">
                                        <a href="" class="btn btn-success" data-dismiss="modal">Go back</a>
                                        <a href="Home.php" class="btn btn-danger">Finish</a>
                                    </div>
                                </div>
                            </div>
                        </div> <!---Modal end--->
                </div>
            </div>


        </div>


        <?php

    }

    else{//there is nothing in the cart




         echo "<script>"."alert('NOTHING ON THE CART!')"."</script>";
             //header("Location: Home.php");


?>
        <div class="container" >

	<!--<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal-1">Activate the button</button>-->
		<div class="modal fade" id="modal-1">
			<div class="modal-dialog modal-lg">
				<div class="modal-content">
					 <div class="modal-header">
					 	<button type="button" class="close" data-dismiss="modal">&times;</button>
					 	<h3 class="modal-title">Error!</h3>
					 </div>
					 <div class="modal-body">
                         There is nothing left in the cart! Please go shopping again....!
                         </div>

					 <div class="modal-footer">
					 	<a href="" class="btn btn-default" data-dismiss="modal">Close</a>

					 </div>
				</div>
			</div>
		</div>
	</div>


<?php

    }
}


else{
header("Location: Home.php");

}


?>





<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="../js/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="../js/bootstrap.min.js"></script>

<script>
$('* div p').each(function () {
var item = $(this).text();
var num = Number(item).toLocaleString('en');

if (Number(item) < 0) {
num = num.replace('-','');
$(this).addClass('negMoney');
}else{
$(this).addClass('enMoney');
}

$(this).text(num);
});
</script>

</body>
</html>


