<?php
/**
 * User: masiya
 * 
 * This is an item page
 */

include 'Connections.php';
session_start();

if(isset($_GET['ItemId']) && !isset($_POST['AddToCart']) && !isset($_POST['Quantity'])){ //user already picked item -no direct access
}
else if(isset($_GET['ItemId']) && isset($_POST['AddToCart']) && isset($_POST['Quantity'])){//user has already picked item and quantity
    $_SESSION['ItemID']=$_GET['ItemId'];
    $_SESSION['Quantity']=$_POST['Quantity'];




    if(isset($_SESSION['UserID'])) {  //user has logged in

        $itID = $_GET['ItemId'];
        $user = $_SESSION['UserID'];
        $qnty=$_SESSION['Quantity'];

        $sql = ("INSERT INTO orders (UserId,ItemId,Quantity) VALUES ('$user','$itID','$qnty')");//query
        mysqli_query($con, $sql);
        header("Location: ItemCarts.php");
    }

    else{ //user has not logged in
        header("Location: login.php");
    }

}
else{//else no item picked - prevent direct access
    header("Location: Home.php");
}


?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>TechGurus</title>

    <!-- Bootstrap -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">
    <link href="../css/custom.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="../js/html5shiv.min.js"></script>
    <script src="../js/respond.min.js"></script>
    <![endif]-->
</head>

<style>
    body{
        padding-top: 40px;
    }
</style>

<body data-spy="scroll" data-target="#my-navbar">

</header>

<!--Navbar-->
   <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation" id="my-navbar">
      <div class="container">
       <div class="navbar-header">
            <a href="Home.php" ><img src="../Images/logo.png" id="logo" class="img-responsive"></a>
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#list-to-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
      </div>
          <div class="collapse navbar-collapse" id="list-to-collapse">

       <ul class="nav navbar-nav">
           <li><li>
           <li><a id="link1" style="color:white;" href="Home.php#Computers" onmouseover="changeColor(this,'#9d9d9d')" onmouseout="changeColor(this,'white')">Computers</a><li>
<li><a id="link2" style="color:white"  href="Home.php#CableAndAdapters" onmouseover="changeColor(this,'#9d9d9d')" onmouseout="changeColor(this,'white')">Adapters & Cables</a><li>
           <li><a id="link3"style="color:white" href="Home.php#Games" onmouseover="changeColor(this,'#9d9d9d')" onmouseout="changeColor(this,'white')">Gaming</a><li>
           <li><a id="link4"style="color:white" href="Home.php#Networking" onmouseover="changeColor(this,'#9d9d9d')" onmouseout="changeColor(this,'white')">Networking</a><li>
           <li><a id="link5" style="color:white;" href="Home.php#Softwares" onmouseover="changeColor(this,'#9d9d9d')" onmouseout="changeColor(this,'white')">Software</a><li>
           <li><a id="link6" style="color:white"  href="Home.php#Speakers" onmouseover="changeColor(this,'#9d9d9d')" onmouseout="changeColor(this,'white')">Speakers</a><li>
           <li><a id="link7"style="color:white" href="Home.php#Storages" onmouseover="changeColor(this,'#9d9d9d')" onmouseout="changeColor(this,'white')">Storage</a><li>
           <li><a id="link8"style="color:white" href="" onmouseover="changeColor(this,'#9d9d9d')" onmouseout="changeColor(this,'white')">Support</a><li>

       </ul>

      </div>
      </div>

   </nav>            <!--Navbar end-->
                <!--Header-->
    <div class="jumbotron text-center">
        <div class="container">
            <h1>TechGurus</h1>
             <h3> We assemble, retail and wholesale Information Technology products and services.</h3>
        </div>
    </div>
<!--header end-->
   </header>


<?php



if(isset($_GET['ItemId'])){

    $_ItemID=$_GET['ItemId'];
    $sql=("select * from item where ItemID='$_ItemID'") ;//query
    $result = mysqli_query($con, $sql);


    $row_cnt = mysqli_num_rows($result);

    if($row_cnt>0){
        $row = mysqli_fetch_array($result, MYSQLI_BOTH);

        ?>


         <div class="container">
             <div class="row">
                    <div class="col-lg-4">
                        <img src="<?php echo "../Images/".$row['Image'];?>" class="img-responsive">
                    </div>
                 <div class="col-lg-5">
                     <?php echo "ID : ".$row['ItemId']."<br>".
                    "Description : ".$row['Description']."<br>".
                     "Price: ".$row['Price']."<br>";
                    ?>

                      <br>
                     <div class="row">
                         <div class="col-lg-3">

                             <form role="form" action="#" method="POST">
                                 <div class="form-group">


                                         <tbody>

                                         <label for="Quantity">Quantity</label>
                                         <input type="text" placeholder="Quantity" name="Quantity" id="Quantity" required>
                                         <br><br>
                                         <button class="btn btn-primary" type="submit" name="AddToCart" id="AddToCart">Add to cart</button>


                                         </tbody>

                                 </div>

                         </div>
                         </form>

                         </div>
                     </div>

                 </div>
             </div>





         </div>
        <?php


    }
    else{

        echo "The item might not be available at the moment!";
    }
    
}
else{


}


?>












<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="../js/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="../js/bootstrap.min.js"></script>
</body>
</html>


