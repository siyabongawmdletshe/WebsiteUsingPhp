<?php
/**
 * User: masiya
 * 
 * logout page
 */

session_start();
unset($_SESSION['UserID']);
session_destroy();
header("Location: Home.php");

?>