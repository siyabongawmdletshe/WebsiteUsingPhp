<?php

/*
* User: masiya 
* This is the Home page
*/ 

session_start();
?>

                                                                                                                                                                                                                                                                                                                                                                                                                          

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>TechGurus</title>

    <!-- Bootstrap -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">
    <link href="../css/custom.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="../js/html5shiv.min.js"></script>
    <script src="../js/respond.min.js"></script>
    <![endif]-->

<!--jquery ui-->
    <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
</head>

<style>
    body{
        padding-top: 40px;
    }
</style>

<body data-spy="scroll" data-target="#my-navbar">

</header>

<!--Navbar-->
   <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation" id="my-navbar">
      <div class="container">
       <div class="navbar-header">
            <a href="Home.php" ><img src="../Images/logo.png" id="logo" class="img-responsive"></a>
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#list-to-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
      </div>
          <div class="collapse navbar-collapse" id="list-to-collapse">

       <ul class="nav navbar-nav">
           <li><li>
           <li><a id="link1" style="color:white;" href="#Computers" onmouseover="changeColor(this,'#9d9d9d')" onmouseout="changeColor(this,'white')">Computers</a><li>
           <li><a id="link2" style="color:white"  href="#CableAndAdapters" onmouseover="changeColor(this,'#9d9d9d')" onmouseout="changeColor(this,'white')">Adapters & Cables</a><li>
           <li><a id="link3"style="color:white" href="#Games" onmouseover="changeColor(this,'#9d9d9d')" onmouseout="changeColor(this,'white')">Gaming</a><li>
           <li><a id="link4"style="color:white" href="#Networking" onmouseover="changeColor(this,'#9d9d9d')" onmouseout="changeColor(this,'white')">Networking</a><li>
           <li><a id="link5" style="color:white;" href="#Softwares" onmouseover="changeColor(this,'#9d9d9d')" onmouseout="changeColor(this,'white')">Software</a><li>
           <li><a id="link6" style="color:white"  href="#Speakers" onmouseover="changeColor(this,'#9d9d9d')" onmouseout="changeColor(this,'white')">Speakers</a><li>
           <li><a id="link7"style="color:white" href="#Storages" onmouseover="changeColor(this,'#9d9d9d')" onmouseout="changeColor(this,'white')">Storage</a><li>
           <li><a id="link8"style="color:white" href="" onmouseover="changeColor(this,'#9d9d9d')" onmouseout="changeColor(this,'white')">Support</a><li>

   <li>
               <div class="dropdown">
                   <button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown"> Account <span class="caret"></span>
                   </button>

                   <ul class="dropdown-menu">
                       <li><a id="link9"style="color:white" href= "<?php if(isset($_SESSION['UserStatus'])){} else { echo "login.php";}?>"
                             type="button" class="btn" <?php if(isset($_SESSION['UserStatus'])){ echo "disabled";}?>>Login <?php if(isset($_SESSION['UserStatus'])){ echo " (".$_SESSION['UserStatus'].")";}
                               else { echo " (not logged in )";}?></a><li>

                       <li><a id="link9"style="color:white" href= "<?php if(!isset($_SESSION['UserStatus'])){} else { echo "UpdateA.php";}?>"
                              type="button" class="btn" <?php if(!isset($_SESSION['UserStatus'])){ echo "disabled";}?>>Edit Profile</a><li>

                       <li><a id="link9"style="color:white" href= "<?php if(!isset($_SESSION['UserStatus'])){} else { echo "logout.php";}?>"
                              type="button" class="btn" <?php if(!isset($_SESSION['UserStatus'])){ echo "disabled";}?>>Logout</a><li>
                   </ul>

               </div>
</li>



       </ul>

              
              
              
              
              
              
              
              
              
              
              
              
              
              
              
              
              
              
              
              
              
              
              
              
              
              
              
              
              
              
              
              
              
              
              
              
              
              
              
              
              
              
              
              
              
              
              
              
              
              
              
              
              
              
              
              
              
              
              
      </div>
      </div>

   </nav>            <!--Navbar end-->
                <!--Header-->
    <div class="jumbotron text-center">
        <div class="container">
            <h1>TechGurus</h1>
             <h3> We assemble, retail and wholesale Information Technology products and services.</h3>
        </div>
    </div>
<!--header end-->
   </header>

<br>

<!--carousels-->
<div class="container">
    <div class="carousel slide" data-ride="carousel" id="carousel-ex">
        <ol class="carousel-indicators" style="background-color: #0f0f0f">             <!--indicators-->
            <li data-target="#carousel-ex" data-slide-to="0" class="active"></li>
            <li data-target="#carousel-ex" data-slide-to="1"></li>
            <li data-target="#carousel-ex" data-slide-to="2"></li>
            <li data-target="#carousel-ex" data-slide-to="3"></li>
            <li data-target="#carousel-ex" data-slide-to="4"></li>
            <li data-target="#carousel-ex" data-slide-to="5"></li>
            <li data-target="#carousel-ex" data-slide-to="6"></li>



        </ol>

                                                                                    <!--images/items-->
        <div class="carousel-inner">
            <div class="item active">
               <a href="#Computers"> <img src="../Images/Computers.PNG" alt="image" class="img-responsive center-block"></a>


                <div class="carousel-caption">
                    <h2>Computers</h2>
                </div>
            </div>


                   <div class="item">
                       <a href="#CableAndAdapters"><img src="../Images/CablesAdapters.PNG" alt="image" class="img-responsive center-block"></a>

                    <div class="carousel-caption">
                        <h2>Adapters & Cables </h2>
                    </div>
                   </div>

            <div class="item">
                <a href="#Games"><img src="../Images/Games.PNG" alt="image" class="img-responsive center-block"></a>
                <div class="carousel-caption">
                    <h2>Gaming </h2>
                </div>
            </div>

            <div class="item">
                <a href="#Networking">  <img src="../Images/Networking.PNG" alt="image" class="img-responsive center-block"></a>
                <div class="carousel-caption">
                    <h2>Networking</h2>
                </div>
            </div>

            <div class="item">
                <a href="#Softwares"> <img src="../Images/Softwares.PNG" alt="image" class="img-responsive center-block"></a>
                <div class="carousel-caption">
                    <h2>Software </h2>
                </div>
            </div>

            <div class="item">
                <a href="#Speakers"><img src="../Images/Speakers.PNG" alt="image" class="img-responsive center-block"></a>
                <div class="carousel-caption">
                    <h2>Speakes </h2>
                </div>
            </div>


            <div class="item">
                <a href="#Storages">   <img src="../Images/Storages.PNG" alt="image" class="img-responsive center-block"></a>>
                <div class="carousel-caption">
                    <h2>Storage </h2>
                </div>
            </div>



            <a href="#carousel-ex" class="left carousel-control" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left"></span>
            </a>
            <a href="#carousel-ex" class="right carousel-control" data-slide="next">
                <span class="glyphicon glyphicon-chevron-right"></span>
            </a>


        </div>
    </div>

</div>

<hr>

<!----menus------------>

<!--Computers-->

<div class="container">

    <div class="page-header" id="Computers">
        <h2>Computers.<small> Check out our awesome deals!</small></h2>
    </div>

    <div class="container">
        <div class="panel-group" id="accordion">


            <div class="panel">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <a href="#collpaseone" data-toggle="collapse" data-parent="#accordion">
                            Destop</a>
                    </h4>
                </div>

                <div id="collpaseone" class="panel-collapse collapse in">
                    <div class="panel-body">
                        <div class="row">

                            <div class="col-md-3">

                                <p class="well">
                                    <img src="../Images/DSTP1.jpg" class="img-responsive">
                                </p>


                            </div>

                            <div class="col-md-3">
                                <p class="well">
                                    <img src="../Images/DSTP2.jpg" class="img-responsive">
                                </p>

                            </div>
                            <div class="col-md-3">

                                <p class="well">
                                    <img src="../Images/DSTP3.jpg" class="img-responsive">
                                </p>


                            </div>
                            <div class="col-md-3">

                                <p class="well">
                                    <img src="../Images/DSTP4.jpg" class="img-responsive">
                                </p>


                            </div>


                        </div><!---ROW END-->

                        <div><!---row 2-->
                            <div class="col-md-3 text-center">
                                <a href="ItemCart.php?ItemId=1" type="button" class="btn btn-primary">Add To Cart</a>
                            </div>
                            <div class="col-md-3 text-center">
                                <a href="ItemCart.php?ItemId=2" type="button" class="btn btn-primary">Add To Cart</a>
                            </div>
                            <div class="col-md-3 text-center">
                                <a href="ItemCart.php?ItemId=3" type="button" class="btn btn-primary">Add To Cart</a>
                            </div>
                            <div class="col-md-3 text-center">
                                <a href="ItemCart.php?ItemId=4" type="button" class="btn btn-primary">Add To Cart</a>
                            </div>
                        </div><!--row 2 end-->



                    </div>


                </div>
            </div>


            <div class="panel">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <a class="accordion-toggle" href="#collpasetwo" data-toggle="collapse" data-parent="#accordion">
                            Laptops</a>
                    </h4>
                </div>
                <div id="collpasetwo" class="panel-collapse collapse">
                    <div class="panel-body">

                        <div class="row">

                            <div class="col-md-3">

                                <p class="well">
                                    <img src="../Images/LP1.jpg" class="img-responsive">
                                </p>


                            </div>

                            <div class="col-md-3">
                                <p class="well">
                                    <img src="../Images/LP2.jpg" class="img-responsive">
                                </p>

                            </div>


                            <div class="col-md-3">
                                <p class="well">
                                    <img src="../Images/LP3.jpg" class="img-responsive">
                                </p>

                            </div>

                            <div class="col-md-3">
                                <p class="well">
                                    <img src="../Images/LP4.jpg" class="img-responsive">
                                </p>

                            </div>

                        </div>   <!---END ROW-->
                        <div><!---row 2-->
                            <div class="col-md-3 text-center">
                                <a href="ItemCart.php?ItemId=5" type="button" class="btn btn-primary">Add To Cart</a>
                            </div>
                            <div class="col-md-3 text-center">
                                <a href="ItemCart.php?ItemId=6" type="button" class="btn btn-primary">Add To Cart</a>
                            </div>
                            <div class="col-md-3 text-center">
                                <a href="ItemCart.php?ItemId=7" type="button" class="btn btn-primary">Add To Cart</a>
                            </div>
                            <div class="col-md-3 text-center">
                                <a href="ItemCart.php?ItemId=8" type="button" class="btn btn-primary">Add To Cart</a>
                            </div>
                        </div><!--row 2 end-->


                    </div>
                </div>
            </div>



        </div>  <!----end container-->
    </div>   <!---end container--->




</div><!--End Container-->


<hr>


<!--Adapters and Cables--->


<div class="container">

    <div class="page-header" id="CableAndAdapters">
        <h2>Adapters and Cables.<small> Check out our awesome deals!</small></h2>
    </div>

    <div class="container">
        <div class="panel-group" id="accordion1">


            <div class="panel">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <a href="#collpaseone1" data-toggle="collapse" data-parent="#accordion1">
                            Adapters</a>
                    </h4>
                </div>

                <div id="collpaseone1" class="panel-collapse collapse in">
                    <div class="panel-body">
                        <div class="row">

                            <div class="col-md-3">

                                <p class="well">
                                    <img src="../Images/ADP1.jpg" class="img-responsive">
                                </p>


                            </div>

                            <div class="col-md-3">
                                <p class="well">
                                    <img src="../Images/ADP2.jpg" class="img-responsive">
                                </p>

                            </div>
                            <div class="col-md-3">

                                <p class="well">
                                    <img src="../Images/ADP3.jpg" class="img-responsive">
                                </p>


                            </div>
                            <div class="col-md-3">

                                <p class="well">
                                    <img src="../Images/ADP4.jpg" class="img-responsive">
                                </p>


                            </div>


                        </div><!---ROW END-->

                        <div><!---row 2-->
                            <div class="col-md-3 text-center">
                                <a href="ItemCart.php?ItemId=9" type="button" class="btn btn-primary">Add To Cart</a>
                            </div>
                            <div class="col-md-3 text-center">
                                <a href="ItemCart.php?ItemId=10" type="button" class="btn btn-primary">Add To Cart</a>
                            </div>
                            <div class="col-md-3 text-center">
                                <a href="ItemCart.php?ItemId=11" type="button" class="btn btn-primary">Add To Cart</a>
                            </div>
                            <div class="col-md-3 text-center">
                                <a href="ItemCart.php?ItemId=12" type="button" class="btn btn-primary">Add To Cart</a>
                            </div>
                        </div><!--row 2 end-->



                    </div>


                </div>
            </div>


            <div class="panel">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <a class="accordion-toggle" href="#collpasetwo1" data-toggle="collapse" data-parent="#accordion1">
                            Display Ports</a>
                    </h4>
                </div>
                <div id="collpasetwo1" class="panel-collapse collapse">
                    <div class="panel-body">

                        <div class="row">

                            <div class="col-md-3">

                                <p class="well">
                                    <img src="../Images/DPP1.jpg" class="img-responsive">
                                </p>


                            </div>

                            <div class="col-md-3">
                                <p class="well">
                                    <img src="../Images/DPP2.jpg" class="img-responsive">
                                </p>

                            </div>


                            <div class="col-md-3">
                                <p class="well">
                                    <img src="../Images/DPP3.jpg" class="img-responsive">
                                </p>

                            </div>

                            <div class="col-md-3">
                                <p class="well">
                                    <img src="../Images/DPP4.jpg" class="img-responsive">
                                </p>

                            </div>

                        </div>   <!---END ROW-->

                        <div><!---row 2-->
                            <div class="col-md-3 text-center">
                                <a href="ItemCart.php?ItemId=13" type="button" class="btn btn-primary">Add To Cart</a>
                            </div>
                            <div class="col-md-3 text-center">
                                <a href="ItemCart.php?ItemId=14" type="button" class="btn btn-primary">Add To Cart</a>
                            </div>
                            <div class="col-md-3 text-center">
                                <a href="ItemCart.php?ItemId=15" type="button" class="btn btn-primary">Add To Cart</a>
                            </div>
                            <div class="col-md-3 text-center">
                                <a href="ItemCart.php?ItemId=16" type="button" class="btn btn-primary">Add To Cart</a>
                            </div>
                        </div><!--row 2 end-->


                    </div>
                </div>
            </div>



        </div>  <!----end container-->
    </div>   <!---end container--->




</div><!--End Container-->



<hr>


<!--Games--->

<div class="container">

    <div class="page-header" id="Games">
        <h2>Gaming.<small> Check out our awesome deals!</small></h2>
    </div>

    <div class="container">
        <div class="panel-group" id="accordion2">


            <div class="panel">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <a href="#collpaseone2" data-toggle="collapse" data-parent="#accordion2">
                            Keyboards</a>
                    </h4>
                </div>

                <div id="collpaseone2" class="panel-collapse collapse in">
                    <div class="panel-body">
                        <div class="row">

                            <div class="col-md-3">

                                <p class="well">
                                    <img src="../Images/GK1.jpg" class="img-responsive">
                                </p>


                            </div>

                            <div class="col-md-3">
                                <p class="well">
                                    <img src="../Images/GK2.jpg" class="img-responsive">
                                </p>

                            </div>
                            <div class="col-md-3">

                                <p class="well">
                                    <img src="../Images/GK3.jpg" class="img-responsive">
                                </p>


                            </div>
                            <div class="col-md-3">

                                <p class="well">
                                    <img src="../Images/GK4.jpg" class="img-responsive">
                                </p>


                            </div>


                        </div><!---ROW END-->
                        <div><!---row 2-->
                            <div class="col-md-3 text-center">
                                <a href="ItemCart.php?ItemId=17" type="button" class="btn btn-primary">Add To Cart</a>
                            </div>
                            <div class="col-md-3 text-center">
                                <a href="ItemCart.php?ItemId=18" type="button" class="btn btn-primary">Add To Cart</a>
                            </div>
                            <div class="col-md-3 text-center">
                                <a href="ItemCart.php?ItemId=19" type="button" class="btn btn-primary">Add To Cart</a>
                            </div>
                            <div class="col-md-3 text-center">
                                <a href="ItemCart.php?ItemId=20" type="button" class="btn btn-primary">Add To Cart</a>
                            </div>
                        </div><!--row 2 end-->



                    </div>


                </div>
            </div>


            <div class="panel">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <a class="accordion-toggle" href="#collpasetwo2" data-toggle="collapse" data-parent="#accordion2">
                            Mouse</a>
                    </h4>
                </div>
                <div id="collpasetwo2" class="panel-collapse collapse">
                    <div class="panel-body">

                        <div class="row">

                            <div class="col-md-3">

                                <p class="well">
                                    <img src="../Images/GM1.jpg" class="img-responsive">
                                </p>


                            </div>

                            <div class="col-md-3">
                                <p class="well">
                                    <img src="../Images/GM2.jpg" class="img-responsive">
                                </p>

                            </div>


                            <div class="col-md-3">
                                <p class="well">
                                    <img src="../Images/GM3.jpg" class="img-responsive">
                                </p>

                            </div>

                            <div class="col-md-3">
                                <p class="well">
                                    <img src="../Images/GM4.jpg" class="img-responsive">
                                </p>

                            </div>

                        </div>   <!---END ROW-->

                        <div><!---row 2-->
                            <div class="col-md-3 text-center">
                                <a href="ItemCart.php?ItemId=21" type="button" class="btn btn-primary">Add To Cart</a>
                            </div>
                            <div class="col-md-3 text-center">
                                <a href="ItemCart.php?ItemId=22" type="button" class="btn btn-primary">Add To Cart</a>
                            </div>
                            <div class="col-md-3 text-center">
                                <a href="ItemCart.php?ItemId=23" type="button" class="btn btn-primary">Add To Cart</a>
                            </div>
                            <div class="col-md-3 text-center">
                                <a href="ItemCart.php?ItemId=24" type="button" class="btn btn-primary">Add To Cart</a>
                            </div>
                        </div><!--row 2 end-->


                    </div>
                </div>
            </div>


            <div class="panel">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <a class="accordion-toggle" href="#collpasetwo22" data-toggle="collapse" data-parent="#accordion2">
                            GamePads/Joysticks</a>
                    </h4>
                </div>
                <div id="collpasetwo22" class="panel-collapse collapse">
                    <div class="panel-body">

                        <div class="row">

                            <div class="col-md-3">

                                <p class="well">
                                    <img src="../Images/JS1.jpg" class="img-responsive">
                                </p>


                            </div>

                            <div class="col-md-3">
                                <p class="well">
                                    <img src="../Images/JS2.jpg" class="img-responsive">
                                </p>

                            </div>


                            <div class="col-md-3">
                                <p class="well">
                                    <img src="../Images/JS3.jpg" class="img-responsive">
                                </p>

                            </div>

                            <div class="col-md-3">
                                <p class="well">
                                    <img src="../Images/JS4.jpg" class="img-responsive">
                                </p>

                            </div>

                        </div>   <!---END ROW-->

                        <div><!---row 2-->
                            <div class="col-md-3 text-center">
                                <a href="ItemCart.php?ItemId=25" type="button" class="btn btn-primary">Add To Cart</a>
                            </div>
                            <div class="col-md-3 text-center">
                                <a href="ItemCart.php?ItemId=26" type="button" class="btn btn-primary">Add To Cart</a>
                            </div>
                            <div class="col-md-3 text-center">
                                <a href="ItemCart.php?ItemId=27" type="button" class="btn btn-primary">Add To Cart</a>
                            </div>
                            <div class="col-md-3 text-center">
                                <a href="ItemCart.php?ItemId=28" type="button" class="btn btn-primary">Add To Cart</a>
                            </div>
                        </div><!--row 2 end-->


                    </div>
                </div>
            </div>



        </div>  <!----end container-->
    </div>   <!---end container--->




</div><!--End Container-->




<hr>


<!--Networking--->


<div class="container">

    <div class="page-header" id="Networking">
        <h2>Networking.<small> Check out our awesome deals!</small></h2>
    </div>

    <div class="container">
        <div class="panel-group" id="accordion3">


            <div class="panel">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <a href="#collpaseone3" data-toggle="collapse" data-parent="#accordion3">
                            Wireless</a>
                    </h4>
                </div>

                <div id="collpaseone3" class="panel-collapse collapse in">
                    <div class="panel-body">
                        <div class="row">

                            <div class="col-md-3">

                                <p class="well">
                                    <img src="../Images/WL1.jpg" class="img-responsive">
                                </p>


                            </div>

                            <div class="col-md-3">
                                <p class="well">
                                    <img src="../Images/WL2.jpg" class="img-responsive">
                                </p>

                            </div>
                            <div class="col-md-3">

                                <p class="well">
                                    <img src="../Images/WL3.jpg" class="img-responsive">
                                </p>


                            </div>
                            <div class="col-md-3">

                                <p class="well">
                                    <img src="../Images/WL4.png" class="img-responsive">
                                </p>


                            </div>


                        </div><!---ROW END-->

                        <div><!---row 2-->
                            <div class="col-md-3 text-center">
                                <a href="ItemCart.php?ItemId=29" type="button" class="btn btn-primary">Add To Cart</a>
                            </div>
                            <div class="col-md-3 text-center">
                                <a href="ItemCart.php?ItemId=30" type="button" class="btn btn-primary">Add To Cart</a>
                            </div>
                            <div class="col-md-3 text-center">
                                <a href="ItemCart.php?ItemId=31" type="button" class="btn btn-primary">Add To Cart</a>
                            </div>
                            <div class="col-md-3 text-center">
                                <a href="ItemCart.php?ItemId=32" type="button" class="btn btn-primary">Add To Cart</a>
                            </div>
                        </div><!--row 2 end-->


                    </div>


                </div>
            </div>


            <div class="panel">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <a class="accordion-toggle" href="#collpasetwo3" data-toggle="collapse" data-parent="#accordion3">
                            Wired</a>
                    </h4>
                </div>
                <div id="collpasetwo3" class="panel-collapse collapse">
                    <div class="panel-body">

                        <div class="row">

                            <div class="col-md-3">

                                <p class="well">
                                    <img src="../Images/WD1.jpg" class="img-responsive">
                                </p>


                            </div>

                            <div class="col-md-3">
                                <p class="well">
                                    <img src="../Images/WD2.jpg" class="img-responsive">
                                </p>

                            </div>


                            <div class="col-md-3">
                                <p class="well">
                                    <img src="../Images/WD3.JPG" class="img-responsive">
                                </p>

                            </div>

                            <div class="col-md-3">
                                <p class="well">
                                    <img src="../Images/WD4.jpg" class="img-responsive">
                                </p>

                            </div>

                        </div>   <!---END ROW-->
                        <div><!---row 2-->
                            <div class="col-md-3 text-center">
                                <a href="ItemCart.php?ItemId=33" type="button" class="btn btn-primary">Add To Cart</a>
                            </div>
                            <div class="col-md-3 text-center">
                                <a href="ItemCart.php?ItemId=34" type="button" class="btn btn-primary">Add To Cart</a>
                            </div>
                            <div class="col-md-3 text-center">
                                <a href="ItemCart.php?ItemId=35" type="button" class="btn btn-primary">Add To Cart</a>
                            </div>
                            <div class="col-md-3 text-center">
                                <a href="ItemCart.php?ItemId=36" type="button" class="btn btn-primary">Add To Cart</a>
                            </div>
                        </div><!--row 2 end-->

                    </div>
                </div>
            </div>


    </div>  <!----end container-->
</div>   <!---end container--->




</div><!--End Container-->





<hr>


<!--Software--->

<div class="container">

    <div class="page-header" id="Softwares">
        <h2>Softwares.<small> Check out our awesome deals!</small></h2>
    </div>

    <div class="container">
        <div class="panel-group" id="accordion4">


            <div class="panel">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <a href="#collpaseone4" data-toggle="collapse" data-parent="#accordion4">
                            Office Suite</a>
                    </h4>
                </div>

                <div id="collpaseone4" class="panel-collapse collapse in">
                    <div class="panel-body">
                        <div class="row">

                            <div class="col-md-3">

                                <p class="well">
                                    <img src="../Images/Of1.jpg" class="img-responsive">
                                </p>


                            </div>

                            <div class="col-md-3">
                                <p class="well">
                                    <img src="../Images/Of2.jpg" class="img-responsive">
                                </p>

                            </div>
                            <div class="col-md-3">

                                <p class="well">
                                    <img src="../Images/Of3.jpg" class="img-responsive">
                                </p>


                            </div>
                            <div class="col-md-3">

                                <p class="well">
                                    <img src="../Images/Of4.jpg" class="img-responsive">
                                </p>


                            </div>


                        </div><!---ROW END-->
                        <div><!---row 2-->
                            <div class="col-md-3 text-center">
                                <a href="ItemCart.php?ItemId=37" type="button" class="btn btn-primary">Add To Cart</a>
                            </div>
                            <div class="col-md-3 text-center">
                                <a href="ItemCart.php?ItemId=38" type="button" class="btn btn-primary">Add To Cart</a>
                            </div>
                            <div class="col-md-3 text-center">
                                <a href="ItemCart.php?ItemId=39" type="button" class="btn btn-primary">Add To Cart</a>
                            </div>
                            <div class="col-md-3 text-center">
                                <a href="ItemCart.php?ItemId=40" type="button" class="btn btn-primary">Add To Cart</a>
                            </div>
                        </div><!--row 2 end-->



                    </div>


                </div>
            </div>


            <div class="panel">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <a class="accordion-toggle" href="#collpasetwo4" data-toggle="collapse" data-parent="#accordion4">
                            Operating System</a>
                    </h4>
                </div>
                <div id="collpasetwo4" class="panel-collapse collapse">
                    <div class="panel-body">

                        <div class="row">

                            <div class="col-md-3">

                                <p class="well">
                                    <img src="../Images/WN1.jpg" class="img-responsive">
                                </p>


                            </div>

                            <div class="col-md-3">
                                <p class="well">
                                    <img src="../Images/WN2.jpg" class="img-responsive">
                                </p>

                            </div>


                            <div class="col-md-3">
                                <p class="well">
                                    <img src="../Images/WN3.jpg" class="img-responsive">
                                </p>

                            </div>

                            <div class="col-md-3">
                                <p class="well">
                                    <img src="../Images/WN4.jpg" class="img-responsive">
                                </p>

                            </div>

                        </div>   <!---END ROW-->

                        <div><!---row 2-->
                            <div class="col-md-3 text-center">
                                <a href="ItemCart.php?ItemId=41" type="button" class="btn btn-primary">Add To Cart</a>
                            </div>
                            <div class="col-md-3 text-center">
                                <a href="ItemCart.php?ItemId=42" type="button" class="btn btn-primary">Add To Cart</a>
                            </div>
                            <div class="col-md-3 text-center">
                                <a href="ItemCart.php?ItemId=43" type="button" class="btn btn-primary">Add To Cart</a>
                            </div>
                            <div class="col-md-3 text-center">
                                <a href="ItemCart.php?ItemId=44" type="button" class="btn btn-primary">Add To Cart</a>
                            </div>
                        </div><!--row 2 end-->


                    </div>
                </div>
            </div>



            <div class="panel ">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <a class="accordion-toggle" href="#collpasetwoo5" data-toggle="collapse" data-parent="#accordion4">
                            Security</a>
                    </h4>
                </div>
                <div id="collpasetwoo5" class="panel-collapse collapse">
                    <div class="panel-body">

                        <div class="row">

                            <div class="col-md-3">

                                <p class="well">
                                    <img src="../Images/AV1.jpg" class="img-responsive">
                                </p>


                            </div>

                            <div class="col-md-3">
                                <p class="well">
                                    <img src="../Images/Port2.jpg" class="img-responsive">
                                </p>

                            </div>
                            <div class="col-md-3">

                                <p class="well">
                                    <img src="../Images/Port3.jpg" class="img-responsive">
                                </p>


                            </div>
                            <div class="col-md-3">

                                <p class="well">
                                    <img src="../Images/Port4.jpg" class="img-responsive">
                                </p>


                            </div>


                        </div> <!--end row-->

                        <div><!---row 2-->
                            <div class="col-md-3 text-center">
                                <a href="ItemCart.php?ItemId=45" type="button" class="btn btn-primary">Add To Cart</a>
                            </div>
                            <div class="col-md-3 text-center">
                                <a href="ItemCart.php?ItemId=46" type="button" class="btn btn-primary">Add To Cart</a>
                            </div>
                            <div class="col-md-3 text-center">
                                <a href="ItemCart.php?ItemId=47" type="button" class="btn btn-primary">Add To Cart</a>
                            </div>
                            <div class="col-md-3 text-center">
                                <a href="ItemCart.php?ItemId=48" type="button" class="btn btn-primary">Add To Cart</a>
                            </div>
                        </div><!--row 2 end-->


                    </div>
                </div>
            </div>



            </div>
        </div>  <!----end container-->
    </div>   <!---end container--->




</div><!--End Container-->





<hr>

<!--Speakers--->

<div class="container">

    <div class="page-header" id="Speakers">
        <h2>Speakers.<small> Check out our awesome deals!</small></h2>
    </div>

    <div class="container">
        <div class="panel-group" id="accordion5">


            <div class="panel">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <a href="#collpaseone5" data-toggle="collapse" data-parent="#accordion5">
                            Earphones & Headphones</a>
                    </h4>
                </div>

                <div id="collpaseone5" class="panel-collapse collapse in">
                    <div class="panel-body">
                        <div class="row">

                            <div class="col-md-3">

                                <p class="well">
                                    <img src="../Images/HE1.jpg" class="img-responsive">
                                </p>


                            </div>

                            <div class="col-md-3">
                                <p class="well">
                                    <img src="../Images/HE2.jpg" class="img-responsive">
                                </p>

                            </div>
                            <div class="col-md-3">

                                <p class="well">
                                    <img src="../Images/HE3.jpg" class="img-responsive">
                                </p>


                            </div>
                            <div class="col-md-3">

                                <p class="well">
                                    <img src="../Images/HE4.jpg" class="img-responsive">
                                </p>


                            </div>


                        </div><!---ROW END-->

                        <div><!---row 2-->
                            <div class="col-md-3 text-center">
                                <a href="ItemCart.php?ItemId=49" type="button" class="btn btn-primary">Add To Cart</a>
                            </div>
                            <div class="col-md-3 text-center">
                                <a href="ItemCart.php?ItemId=50" type="button" class="btn btn-primary">Add To Cart</a>
                            </div>
                            <div class="col-md-3 text-center">
                                <a href="ItemCart.php?ItemId=51" type="button" class="btn btn-primary">Add To Cart</a>
                            </div>
                            <div class="col-md-3 text-center">
                                <a href="ItemCart.php?ItemId=52" type="button" class="btn btn-primary">Add To Cart</a>
                            </div>
                        </div><!--row 2 end-->



                        </div>


                    </div>
                </div>


            <div class="panel">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <a class="accordion-toggle" href="#collpasetwo6" data-toggle="collapse" data-parent="#accordion5">
                            Power Speakers</a>
                    </h4>
                </div>
                <div id="collpasetwo6" class="panel-collapse collapse">
                    <div class="panel-body">

                        <div class="row">

                            <div class="col-md-3">

                                <p class="well">
                                    <img src="../Images/PS1.jpg" class="img-responsive">
                                </p>


                            </div>

                            <div class="col-md-3">
                                <p class="well">
                                    <img src="../Images/PS2.png" class="img-responsive">
                                </p>

                            </div>


                            <div class="col-md-3">
                                <p class="well">
                                    <img src="../Images/PS3.jpg" class="img-responsive">
                                </p>

                            </div>

                            <div class="col-md-3">
                                <p class="well">
                                    <img src="../Images/PS4.png" class="img-responsive">
                                </p>

                            </div>

                        </div>   <!---END ROW-->

                        <div><!---row 2-->
                            <div class="col-md-3 text-center">
                                <a href="ItemCart.php?ItemId=53" type="button" class="btn btn-primary">Add To Cart</a>
                            </div>
                            <div class="col-md-3 text-center">
                                <a href="ItemCart.php?ItemId=54" type="button" class="btn btn-primary">Add To Cart</a>
                            </div>
                            <div class="col-md-3 text-center">
                                <a href="ItemCart.php?ItemId=55" type="button" class="btn btn-primary">Add To Cart</a>
                            </div>
                            <div class="col-md-3 text-center">
                                <a href="ItemCart.php?ItemId=56" type="button" class="btn btn-primary">Add To Cart</a>
                            </div>
                        </div><!--row 2 end-->


                    </div>
                </div>
            </div>



            <div class="panel ">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <a class="accordion-toggle" href="#collpasetwo5" data-toggle="collapse" data-parent="#accordion5">
                            Portable Speakers</a>
                    </h4>
                </div>
                <div id="collpasetwo5" class="panel-collapse collapse">
                    <div class="panel-body">

                        <div class="row">

                            <div class="col-md-3">

                                <p class="well">
                                    <img src="../Images/Port1.jpg" class="img-responsive">
                                </p>


                            </div>

                            <div class="col-md-3">
                                <p class="well">
                                    <img src="../Images/Port2.jpg" class="img-responsive">
                                </p>

                            </div>
                            <div class="col-md-3">

                                <p class="well">
                                    <img src="../Images/Port3.jpg" class="img-responsive">
                                </p>


                            </div>
                            <div class="col-md-3">

                                <p class="well">
                                    <img src="../Images/Port4.jpg" class="img-responsive">
                                </p>


                            </div>


                        </div> <!--end row-->

                        <div><!---row 2-->
                            <div class="col-md-3 text-center">
                                <a href="ItemCart.php?ItemId=57" type="button" class="btn btn-primary">Add To Cart</a>
                            </div>
                            <div class="col-md-3 text-center">
                                <a href="ItemCart.php?ItemId=58" type="button" class="btn btn-primary">Add To Cart</a>
                            </div>
                            <div class="col-md-3 text-center">
                                <a href="ItemCart.php?ItemId=59" type="button" class="btn btn-primary">Add To Cart</a>
                            </div>
                            <div class="col-md-3 text-center">
                                <a href="ItemCart.php?ItemId=60" type="button" class="btn btn-primary">Add To Cart</a>
                            </div>
                        </div><!--row 2 end-->


                    </div>
                </div>
            </div>


            <div class="panel ">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <a class="accordion-toggle" href="#collpasetwo11" data-toggle="collapse" data-parent="#accordion5">
                            USB Speakers</a>
                    </h4>
                </div>
                <div id="collpasetwo11" class="panel-collapse collapse">
                    <div class="panel-body">

                        <div class="row">

                            <div class="col-md-3">

                                <p class="well">
                                    <img src="../Images/UsbS1.jpg" class="img-responsive">
                                </p>


                            </div>

                            <div class="col-md-3">
                                <p class="well">
                                    <img src="../Images/UsbS2.jpg" class="img-responsive">
                                </p>

                            </div>
                            <div class="col-md-3">

                                <p class="well">
                                    <img src="../Images/UsbS3.jpg" class="img-responsive">
                                </p>


                            </div>
                            <div class="col-md-3">

                                <p class="well">
                                    <img src="../Images/UsbS4.jpg" class="img-responsive">
                                </p>


                            </div>


                        </div>   <!---ROW END-->

                        <div><!---row 2-->
                            <div class="col-md-3 text-center">
                                <a href="ItemCart.php?ItemId=61" type="button" class="btn btn-primary">Add To Cart</a>
                            </div>
                            <div class="col-md-3 text-center">
                                <a href="ItemCart.php?ItemId=62" type="button" class="btn btn-primary">Add To Cart</a>
                            </div>
                            <div class="col-md-3 text-center">
                                <a href="ItemCart.php?ItemId=63" type="button" class="btn btn-primary">Add To Cart</a>
                            </div>
                            <div class="col-md-3 text-center">
                                <a href="ItemCart.php?ItemId=64" type="button" class="btn btn-primary">Add To Cart</a>
                            </div>
                        </div><!--row 2 end-->



                    </div>
                </div>
            </div>
        </div>  <!----end container-->
        </div>   <!---end container--->




</div><!--End Container-->



<hr>


<!--Storages--->

<div class="container">

    <div class="page-header" id="Storages">
        <h2>Storages.<small> Check out our awesome deals!</small></h2>
    </div>

    <div class="container">
        <div class="panel-group" id="accordion6">


            <div class="panel">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <a href="#collpaseone6" data-toggle="collapse" data-parent="#accordion6">
                            Desktop HDD</a>
                    </h4>
                </div>

                <div id="collpaseone6" class="panel-collapse collapse in">
                    <div class="panel-body">
                        <div class="row">

                            <div class="col-md-3">

                                <p class="well">
                                    <img src="../Images/DH1.jpg" class="img-responsive">
                                </p>


                            </div>

                            <div class="col-md-3">
                                <p class="well">
                                    <img src="../Images/DH2.jpg" class="img-responsive">
                                </p>

                            </div>
                            <div class="col-md-3">

                                <p class="well">
                                    <img src="../Images/DH3.jpg" class="img-responsive">
                                </p>


                            </div>
                            <div class="col-md-3">

                                <p class="well">
                                    <img src="../Images/DH4.jpg" class="img-responsive">
                                </p>


                            </div>


                        </div><!---ROW END-->

                        <div><!---row 2-->
                            <div class="col-md-3 text-center">
                                <a href="ItemCart.php?ItemId=65" type="button" class="btn btn-primary">Add To Cart</a>
                            </div>
                            <div class="col-md-3 text-center">
                                <a href="ItemCart.php?ItemId=66" type="button" class="btn btn-primary">Add To Cart</a>
                            </div>
                            <div class="col-md-3 text-center">
                                <a href="ItemCart.php?ItemId=67" type="button" class="btn btn-primary">Add To Cart</a>
                            </div>
                            <div class="col-md-3 text-center">
                                <a href="ItemCart.php?ItemId=68" type="button" class="btn btn-primary">Add To Cart</a>
                            </div>
                        </div><!--row 2 end-->



                    </div>


                </div>
            </div>
        <!--not here---->

        <div class="panel">
            <div id="ItemPanel" class="panel-heading">
                <h4 class="panel-title">
                    <a class="accordion-toggle" href="#collpasetwo7" data-toggle="collapse" data-parent="#accordion6">
                        Laptops HDD</a>
                </h4>
            </div>
            <div id="collpasetwo7" class="panel-collapse collapse">
                <div class="panel-body">

                    <div class="row">

                        <div class="col-md-3">

                            <p class="well">
                                <img src="../Images/LH1.jpg" class="img-responsive">
                            </p>


                        </div>

                        <div class="col-md-3">
                            <p class="well">
                                <img src="../Images/LH2.jpg" class="img-responsive">
                            </p>

                        </div>

                    </div>   <!---END ROW-->

                    <div><!---row 2-->
                        <div class="col-md-3 text-center">
                            <a href="ItemCart.php?ItemId=69" type="button" class="btn btn-primary">Add To Cart</a>
                        </div>
                        <div class="col-md-3 text-center">
                            <a href="ItemCart.php?ItemId=70" type="button" class="btn btn-primary">Add To Cart</a>
                        </div>

                        </div>
                    </div><!--row 2 end-->

                </div>>
            </div>
        </div>



        <div class="panel ">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a class="accordion-toggle" href="#collpasetwo8" data-toggle="collapse" data-parent="#accordion6">
                        External Hard Drives</a>
                </h4>
            </div>
            <div id="collpasetwo8" class="panel-collapse collapse">
                <div class="panel-body">

                    <div class="row">

                        <div class="col-md-3">

                            <p class="well">
                                <img src="../Images/EXH1.jpg" class="img-responsive">
                            </p>


                        </div>

                        <div class="col-md-3">
                            <p class="well">
                                <img src="../Images/EXH2.jpg" class="img-responsive">
                                Seagate Expansion 2TB External Portable 2.5 Hard Drive USB 3.0
                                R 1,699.00
                            </p>

                        </div>
                        <div class="col-md-3">

                            <p class="well">
                                <img src="../Images/EXH3.jpg" class="img-responsive">
                            </p>


                        </div>
                        <div class="col-md-3">

                            <p class="well">
                                <img src="../Images/EXH4.jpg" class="img-responsive">
                            </p>


                        </div>


                    </div> <!--end row-->


                    <div><!---row 2-->
                        <div class="col-md-3 text-center">
                            <a href="ItemCart.php?ItemId=71" type="button" class="btn btn-primary">Add To Cart</a>
                        </div>
                        <div class="col-md-3 text-center">
                            <a href="ItemCart.php?ItemId=72" type="button" class="btn btn-primary">Add To Cart</a>
                        </div>
                        <div class="col-md-3 text-center">
                            <a href="ItemCart.php?ItemId=73" type="button" class="btn btn-primary">Add To Cart</a>
                        </div>
                        <div class="col-md-3 text-center">
                            <a href="ItemCart.php?ItemId=74" type="button" class="btn btn-primary">Add To Cart</a>
                        </div>
                    </div><!--row 2 end-->
                </div>
            </div>
        </div>


        <div class="panel ">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a class="accordion-toggle" href="#collpasetwo9" data-toggle="collapse" data-parent="#accordion6">
                        Memory Card</a>
                </h4>
            </div>
            <div id="collpasetwo9" class="panel-collapse collapse">
                <div class="panel-body">

                    <div class="row">

                        <div class="col-md-3">

                            <p class="well">
                                <img src="../Images/MC1.jpg" class="img-responsive">
                            </p>


                        </div>

                        <div class="col-md-3">
                            <p class="well">
                                <img src="../Images/MC2.jpg" class="img-responsive">
                            </p>

                        </div>
                        <div class="col-md-3">

                            <p class="well">
                                <img src="../Images/MC3.jpg" class="img-responsive">
                            </p>


                        </div>
                        <div class="col-md-3">

                            <p class="well">
                                <img src="../Images/MC4.jpg" class="img-responsive">
                            </p>


                        </div>


                    </div>   <!---ROW END-->

                    <div><!---row 2-->
                        <div class="col-md-3 text-center">
                            <a href="ItemCart.php?ItemId=75" type="button" class="btn btn-primary">Add To Cart</a>
                        </div>
                        <div class="col-md-3 text-center">
                            <a href="ItemCart.php?ItemId=76" type="button" class="btn btn-primary">Add To Cart</a>
                        </div>
                        <div class="col-md-3 text-center">
                            <a href="ItemCart.php?ItemId=77" type="button" class="btn btn-primary">Add To Cart</a>
                        </div>
                        <div class="col-md-3 text-center">
                            <a href="ItemCart.php?ItemId=78" type="button" class="btn btn-primary">Add To Cart</a>
                        </div>
                    </div><!--row 2 end-->



                </div>
            </div>
        </div>



        <div class="panel ">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a class="accordion-toggle" href="#collpasetwo10" data-toggle="collapse" data-parent="#accordion6">
                        Memory Sticks</a>
                </h4>
            </div>
            <div id="collpasetwo10" class="panel-collapse collapse">
                <div class="panel-body">


                    <div class="row">

                        <div class="col-md-3">

                            <p class="well">
                                <img src="../Images/MS1.jpg" class="img-responsive">
                            </p>


                        </div>

                        <div class="col-md-3">
                            <p class="well">
                                <img src="../Images/MS2.jpg" class="img-responsive">
                            </p>

                        </div>
                        <div class="col-md-3">

                            <p class="well">
                                <img src="../Images/MS3.jpg" class="img-responsive">
                            </p>


                        </div>
                        <div class="col-md-3">

                            <p class="well">
                                <img src="../Images/MS4.jpg" class="img-responsive">
                            </p>


                        </div>


                    </div>    <!--end row-->


                    <div><!---row 2-->
                        <div class="col-md-3 text-center">
                            <a href="ItemCart.php?ItemId=79" type="button" class="btn btn-primary">Add To Cart</a>
                        </div>
                        <div class="col-md-3 text-center">
                            <a href="ItemCart.php?ItemId=80" type="button" class="btn btn-primary">Add To Cart</a>
                        </div>
                        <div class="col-md-3 text-center">
                            <a href="ItemCart.php?ItemId=81" type="button" class="btn btn-primary">Add To Cart</a>
                        </div>
                        <div class="col-md-3 text-center">
                            <a href="ItemCart.php?ItemId=82" type="button" class="btn btn-primary">Add To Cart</a>
                        </div>
                    </div><!--row 2 end-->





                </div>
            </div>
        </div>




    </div>   <!---end container--->


</div> <!-- end container-->

</div><!--End Container-->



<hr>




<!-- Change the color of the element-->
   <script type="text/javascript" language="javascript">
       function changeColor(idObj,colorObj)
       {
           document.getElementById(idObj.id).style.color = colorObj;
       }
   </script>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="../js/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="../js/bootstrap.min.js"></script>


<script type="text/javascript">


    $("document").ready(function () {

        $("#mytabs").tabs({

            event: "click",
            show: "fadeIn",
            hide: "fadeOut",
            collapsible: true,
            active: 1,
            heightStyle: "content"
        });



    });


</script>







</body>
</html>