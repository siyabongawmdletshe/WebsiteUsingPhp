<?php
/**
 * User: masiya
 *
 * This is the login page
 */

include 'Connections.php';
session_start();

?>


<?php




if(isset($_POST['login'])){ //login

    $LogEmail=mysqli_real_escape_string($con,$_POST['loginemail']);
    $LogPassW=mysqli_real_escape_string($con,$_POST['loginpassword']);

        $sql=("select * from user where Email='$LogEmail'");

        $Logresult = mysqli_query($con, $sql);

        /* determine number of rows result set */
        $row_cnt = mysqli_num_rows($Logresult);

        if($row_cnt>0){//User exists
            $row = mysqli_fetch_array($Logresult, MYSQLI_BOTH);

            if(password_verify($LogPassW,$row["Password"])){

                $_SESSION["UserID"]=$row['UserID'];
                $_SESSION["FName"]=$row["Fname"];
                $_SESSION["LName"]=$row["Lname"];
                $_SESSION["Email"]=$row["Email"];
                $_SESSION["Password"]=$LogPassW;
                $_SESSION["ImageSrc"]=$row["Profile_Image"];
                $_SESSION['UserStatus'] = "You're logged in";


                    //user has successfully logged in.  Now go to cart page to continue with the order
                if(isset( $_SESSION['ItemID']) && isset( $_SESSION['Quantity'])){


                        $itID = $_SESSION['ItemID'];
                        $user = $_SESSION['UserID'];
                        $qnty=$_SESSION['Quantity'];
                        $sql = ("INSERT INTO orders (UserId,ItemId,Quantity) VALUES ('$user','$itID',$qnty)");//query
                        mysqli_query($con, $sql);
                        header("Location: ItemCarts.php");
                 }
                else{//Go home no order yet
                    $_SESSION['UserStatus'] = "You're logged in";
                    header('Location: Home.php');

                }




            }
            else if(!password_verify($PassW,$row["Password"])){
                $_SESSION["loginFail"]="Login failed!! Please enter the correct password.";
                $_SESSION["InvalidUser"]="";
                $_SESSION["InPutMissing"]="";
                $_SESSION["UserEmail"]=$row["Email"];
            }

        }

        //number of rows returned is zero
        else{
            $_SESSION["InvalidUser"]="Your account does not exit! Please sign up first.";
            $_SESSION["LoginFail"]="";
            $_SESSION["InPutMissing"]="";

        }

}

//registering


if(isset($_POST['Register']))
{

    $FName=mysqli_real_escape_string($con,$_POST['firstname']);
    $LName=mysqli_real_escape_string($con,$_POST['lastname']);
    $Email=mysqli_real_escape_string($con,$_POST['email']);
    $PassW=mysqli_real_escape_string($con,$_POST['password']);

    $store_password=password_hash($PassW,PASSWORD_BCRYPT,array('cost'=>10));//incrypt the password

    $sql=("select * from user where Email='$Email'") ;//query
    $result = mysqli_query($con, $sql);


    $row_cnt = mysqli_num_rows($result);/* determine number of rows result set */

    if($row_cnt>0){//EMAIL EXISTS

        $_SESSION["UserEmailExists"]="An account with this email already exists! Try a different one.";
        $_SESSION["InPutMissing"]="";
        $_SESSION["InvalidEmail"]= "";

    }
    else{


        //No error - registration was successful
        $sql=("INSERT INTO user (Fname,Lname,Email,Password) Values('$FName','$LName','$Email','$store_password')");
        mysqli_query($con,$sql);
        mysqli_close($con);
        //header("Location:login2.php");

    }

}



?>



<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>TechGurus</title>

    <!-- Bootstrap -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">
    <link href="../css/custom.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="../js/html5shiv.min.js"></script>
    <script src="../js/respond.min.js"></script>
    <![endif]-->
</head>

<style>
    body{
        padding-top: 40px;
    }
</style>

<body data-spy="scroll" data-target="#my-navbar">

</header>

<!--Navbar-->
   <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation" id="my-navbar">
      <div class="container">
       <div class="navbar-header">
            <a href="Home.php" ><img src="../Images/logo.png" id="logo" class="img-responsive"></a>
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#list-to-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
      </div>
          <div class="collapse navbar-collapse" id="list-to-collapse">

       <ul class="nav navbar-nav">
           <li><li>
           <li><a id="link1" style="color:white;" href="Home.php#Computers" onmouseover="changeColor(this,'#9d9d9d')" onmouseout="changeColor(this,'white')">Computers</a><li>
<li><a id="link2" style="color:white"  href="Home.php#CableAndAdapters" onmouseover="changeColor(this,'#9d9d9d')" onmouseout="changeColor(this,'white')">Adapters & Cables</a><li>
           <li><a id="link3"style="color:white" href="Home.php#Games" onmouseover="changeColor(this,'#9d9d9d')" onmouseout="changeColor(this,'white')">Gaming</a><li>
           <li><a id="link4"style="color:white" href="Home.php#Networking" onmouseover="changeColor(this,'#9d9d9d')" onmouseout="changeColor(this,'white')">Networking</a><li>
           <li><a id="link5" style="color:white;" href="Home.php#Softwares" onmouseover="changeColor(this,'#9d9d9d')" onmouseout="changeColor(this,'white')">Software</a><li>
           <li><a id="link6" style="color:white"  href="Home.php#Speakers" onmouseover="changeColor(this,'#9d9d9d')" onmouseout="changeColor(this,'white')">Speakers</a><li>
           <li><a id="link7"style="color:white" href="Home.php#Storages" onmouseover="changeColor(this,'#9d9d9d')" onmouseout="changeColor(this,'white')">Storage</a><li>
           <li><a id="link8"style="color:white" href="" onmouseover="changeColor(this,'#9d9d9d')" onmouseout="changeColor(this,'white')">Support</a><li>

       </ul>

      </div>
      </div>

   </nav>            <!--Navbar end-->
                <!--Header-->
    <div class="jumbotron text-center">
        <div class="container">
            <h1>TechGurus</h1>
             <h3> We assemble, retail and wholesale Information Technology products and services.</h3>
        </div>
    </div>
<!--header end-->
   </header>




         <div class="container">

                     <div class="row">
                         <div class="col-lg-6">

                             <form role="form" action="#" method="post">
                                 <div class="form-group">

                                     <table class="table table-bordered">

                                         <tbody>
                                         <tr>
                                             <th>Already have an account? Sign in here</th>
                                         </tr>
                                         <tr>
                                             <th><input type="text" class="form-control" name="loginemail" id="loginemail"
                                                        placeholder="Enter email" required></th>
                                         </tr>
                                         <tr>
                                             <th><input type="password" class="form-control" name="loginpassword" id="loginpassword"
                                                        placeholder="Enter password" required> </th>
                                         </tr>

                                         </tbody>

                                     </table>

                                     <div class="checkbox">
                                         <label>
                                             <input type="checkbox"> Remember me
                                         </label>
                                     </div>

                                     <button type="submit" name="login" class="btn btn-success">Sign in</button>


                                 </div>
                                 <a href="ForgotP.php" type="button" name="ForgotPassword" class="btn btn-info">Forgot password?</a>


                         </form>

                       </div>



                         <div class="row">
                             <div class="col-lg-6">

                                 <form role="form" method="post" action="#">
                                     <div class="form-group">

                                         <table class="table table-bordered">

                                             <tbody>
                                             <tr>
                                                 <th>Don't have an account? Register here</th>
                                             </tr>
                                             <tr>
                                                 <th><input type="text" class="form-control" name="firstname" id="first name"
                                                            placeholder="Enter First name" required></th>
                                             </tr>
                                             <tr>
                                                 <th><input type="text" class="form-control" name="lastname"id="last name"
                                                            placeholder="Enter Last name" required></th>
                                             </tr>
                                             <tr>
                                                 <th><input type="text" class="form-control"  name="email" id="email"
                                                            placeholder="Enter email" required></th>
                                             </tr>
                                             <tr>
                                                 <th><input type="password" class="form-control" name ="password" id="password"
                                                            placeholder="Enter password" required></th>
                                             </tr>

                                             </tbody>

                                         </table>

                                         <div class="checkbox">
                                             <label>
                                                 <input type="checkbox"> Remember me
                                             </label>
                                         </div>

                                         <button type="submit" name="Register" class="btn btn-success">Sign up</button>


                                     </div>


                                 </form>

                             </div>


             </div>





         </div>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="../js/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="../js/bootstrap.min.js"></script>
</body>
</html>


