<?php
/**
 * User: masiya
 * 
 * This is the user account update page
 */

include 'Connections.php';
session_start();
?>

<?php

if(isset($_SESSION["UserID"])){
}

else{
    header('Location:Home.php');
}

?>


<?php
$user =$_SESSION["UserID"];
$sql=("select* from user where UserID='$user'");
$result = mysqli_query($con, $sql);
$row = mysqli_fetch_array($result, MYSQLI_BOTH);

$_SESSION["FName"]=$row["Fname"];
$_SESSION["LName"]=$row["Lname"];
$_SESSION["Email"]=$row["Email"];
$_SESSION["NewPassword"]=$_SESSION["Password"];
$_SESSION["ImageSrc"]=$row['Profile_Image'];

?>


<?php

if(isset($_POST['Update'])){ //update without uploading the profile picture

    $FName=$_POST['First_Name'];
    $LName=$_POST['Last_Name'];
    $Email=$_POST['Email'];
    $PassW=$_POST['Password'];
    $storepassword=password_hash($PassW,PASSWORD_BCRYPT,array('cost'=>10));

    $sql=("Update user set Fname='$FName',Lname='$LName',Email='$Email',Password='$storepassword'
	where UserID='$user'");
    mysqli_query($con,$sql);
    $_SESSION["FName"]=$FName;
    $_SESSION["LName"]=$LName;
    $_SESSION["Email"]=$Email;

    

}

if(isset($_POST['Update']) && isset($_SESSION["UpdatedImageSrc"]) ){ //update with uploaded userprofile picture

    $FName=$_POST['First_Name'];
    $LName=$_POST['Last_Name'];
    $Email=$_POST['Email'];
    $PassW=$_POST['Password'];
    $propic=$_SESSION["UpdatedImageSrc"];
    $_SESSION["ImageSrc"]=$propic;
    $storepassword=password_hash($PassW,PASSWORD_BCRYPT,array('cost'=>10));

    $sql=("Update user set Fname='$FName',Lname='$LName',Email='$Email',Password='$storepassword',Profile_Image	='$propic'
	where UserID='$user'");
    mysqli_query($con,$sql);
    $_SESSION["FName"]=$FName;
    $_SESSION["LName"]=$LName;
    $_SESSION["Email"]=$Email;

    //header("Location:U");

}


?>

<?php

if(isset($_POST['UploadImage'])){	//Load image
    $target_dir = "../Images/Uploads/";
    $target_file = $target_dir.basename($_FILES["fileToUpload"]["name"]);
    $uploadOk = 1;
    $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);

// Check if the file is a actual real or fake
    if(isset($_POST["submit"])) {
        //$check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
        $finfo = finfo_open(FILEINFO_MIME_TYPE);

        $mime = finfo_file($finfo, $_FILES["fileToUpload"]["tmp_name"]);

        if($mime !== false) {
            //echo "File is an image - " . $mime.".";
            $uploadOk = 1;
        } else {
            $_SESSION["NotAnImmageError"]="File is not an image.";
            //echo "File is not an image.";
            $uploadOk = 0;
        }
    }
// Check if file already exists
    if (file_exists($target_file)) {
        $target_file= $target_dir.basename($_FILES["fileToUpload"]["name"]);
        //echo "Sorry, file already exists.";
        //$uploadOk = 0;
    }
// Check file size
    if ($_FILES["fileToUpload"]["size"] > 50000000) {
        $_SESSION["SizeError"]="Sorry, your file is too large.";
        //echo "Sorry, your file is too large.";
        $uploadOk = 0;
    }
// Allow certain file formats
    if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif" ) {
        $_SESSION["TypeError"]="Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
        //echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
        $uploadOk = 0;
    }
// Check if $uploadOk is set to 0 by an error
    if ($uploadOk == 0) {
        $_SESSION["UploadError"]="Sorry, your file was not uploaded.";
        //echo "Sorry, your file was not uploaded.";
// if everything is ok, try to upload file
    } else {
        if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
            $filename=basename( $_FILES["fileToUpload"]["name"]);

            //echo "The file ".basename( $_FILES["fileToUpload"]["name"]). " has been uploaded.";
            //echo "<img src=$target_dir$filename alt=HTML5 Icon style=width:100px;height:100px;  style=text-align: left; vertical-align: top;>";
            $_SESSION["ImageSrc"]=$target_dir.$filename;
            $_SESSION["UpdatedImageSrc"]=$target_dir.$filename;
            $_SESSION["UploadWasOk"]="The image was successfully uploaded!";



        } else {
            $_SESSION["UploadError1"]="Sorry, there was an error uploading your file..";
            //echo "Sorry, there was an error uploading your file.";
        }
    }

}
?>







<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/html">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>TechGurus</title>

    <!-- Bootstrap -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">
    <link href="../css/custom.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="../js/html5shiv.min.js"></script>
    <script src="../js/respond.min.js"></script>
    <![endif]-->
</head>

<style>
    body{
        padding-top: 40px;
    }
</style>

<body data-spy="scroll" data-target="#my-navbar">

</header>

<!--Navbar-->
   <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation" id="my-navbar">
      <div class="container">
       <div class="navbar-header">
            <a href="Home.php" ><img src="../Images/logo.png" id="logo" class="img-responsive"></a>
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#list-to-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
      </div>
          <div class="collapse navbar-collapse" id="list-to-collapse">

       <ul class="nav navbar-nav">
           <li><li>
           <li><a id="link1" style="color:white;" href="Home.php#Computers" onmouseover="changeColor(this,'#9d9d9d')" onmouseout="changeColor(this,'white')">Computers</a><li>
<li><a id="link2" style="color:white"  href="Home.php#CableAndAdapters" onmouseover="changeColor(this,'#9d9d9d')" onmouseout="changeColor(this,'white')">Adapters & Cables</a><li>
           <li><a id="link3"style="color:white" href="Home.php#Games" onmouseover="changeColor(this,'#9d9d9d')" onmouseout="changeColor(this,'white')">Gaming</a><li>
           <li><a id="link4"style="color:white" href="Home.php#Networking" onmouseover="changeColor(this,'#9d9d9d')" onmouseout="changeColor(this,'white')">Networking</a><li>
           <li><a id="link5" style="color:white;" href="Home.php#Softwares" onmouseover="changeColor(this,'#9d9d9d')" onmouseout="changeColor(this,'white')">Software</a><li>
           <li><a id="link6" style="color:white"  href="Home.php#Speakers" onmouseover="changeColor(this,'#9d9d9d')" onmouseout="changeColor(this,'white')">Speakers</a><li>
           <li><a id="link7"style="color:white" href="Home.php#Storages" onmouseover="changeColor(this,'#9d9d9d')" onmouseout="changeColor(this,'white')">Storage</a><li>
           <li><a id="link8"style="color:white" href="" onmouseover="changeColor(this,'#9d9d9d')" onmouseout="changeColor(this,'white')">Support</a><li>


               <div class="dropdown">
                   <button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown"> Account <span class="caret"></span>
                   </button>

                   <ul class="dropdown-menu">
                       <li><a id="link9"style="color:white" href= "<?php if(isset($_SESSION['UserStatus'])){} else { echo "login.php";}?>"
                              type="button" class="btn" <?php if(isset($_SESSION['UserStatus'])){ echo "disabled";}?>>Login <?php if(isset($_SESSION['UserStatus'])){ echo " (".$_SESSION['UserStatus'].")";}
                               else { echo " (not logged in )";}?></a><li>

                       <li><a id="link9"style="color:white" href= "<?php if(!isset($_SESSION['UserStatus'])){} else { echo "UpdateA.php";}?>"
                              type="button" class="btn" <?php if(!isset($_SESSION['UserStatus'])){ echo "disabled";}?>>Edit Profile</a><li>

                       <li><a id="link9"style="color:white" href= "<?php if(!isset($_SESSION['UserStatus'])){} else { echo "logout.php";}?>"
                              type="button" class="btn" <?php if(!isset($_SESSION['UserStatus'])){ echo "disabled";}?>>Logout</a><li>
                   </ul>

               </div>
           </li>


       </ul>

      </div>
      </div>

   </nav>            <!--Navbar end-->
                <!--Header-->
    <div class="jumbotron text-center">
        <div class="container">
            <h1>TechGurus</h1>
             <h3> We assemble, retail and wholesale Information Technology products and services.</h3>
        </div>
    </div>
<!--header end-->
   </header>


<div class="container">

<div class="row">
    <div class="col-md-6">
    <form role="form" action="" method="POST">
        
        <div class="form-group">

            <table class="table table-bordered">
                <tbody>
            
               <tr><td>
                     <input type="text" name="First_Name" class="form-control" id="First_Name" value="<?php echo $_SESSION["FName"] ;?>" placeholder="First name" required />
                   </td></tr>
               <tr><td>
                       <input type="text" name="Last_Name"  class="form-control" id="Last_Name" value="<?php echo $_SESSION["LName"];?>" placeholder="Last name" required/>
               </td></tr>
               <tr><td>
                       <input type="text" name="Email"  class="form-control" id="Email" value="<?php echo $_SESSION["Email"];?>" placeholder="Email" required/>
               </td></tr>
               <tr><td>
               <input type="password" name="Password"  class="form-control" id="Password" placeholder="Enter old/new password" required/>
               </td></tr>

             </tbody>
            </table>
            <input type="submit" name="Update"  class="btn btn-primary" id="Update" value="Update details">


</div>
    </form>
    </div>

    <div class="col-md-6">
        <img src="<?php echo $_SESSION["ImageSrc"]?>" alt="ProfilePic" onError="this.onerror=null;this.src='../Images/Uploads/P1.png';"  width="125px" height="125px"/>

        <form action="" method="post" enctype="multipart/form-data">

            <div class="FormElement">
                <br>
                <b style="color:orange">Profile Picture(optional):</b>
                <input type="file" name="fileToUpload" id="fileToUpload" class="file">
                <input type="submit" value="Upload Image" name="UploadImage" class="button1" id="UploadImage">

            </div>

            <div class="FormElement">
                <p style="font: italic bold 12px/30px Georgia, serif;color:red;">
                    <?php  if(isset($_SESSION["NotAnImmageError"]) && $_SESSION["NotAnImmageError"]!="")
                    { echo $_SESSION["NotAnImmageError"]; $_SESSION["NotAnImmageError"]="";}
                    ?>

                    <?php  if(isset($_SESSION["SizeError"]) && $_SESSION["SizeError"]!="")
                    { echo $_SESSION["SizeError"]; $_SESSION["SizeError"]="";}
                    ?>
                    <?php  if(isset($_SESSION["UploadError"]) && $_SESSION["UploadError"]!="")
                    { echo $_SESSION["UploadError"]; $_SESSION["UploadError"]="";}
                    ?>
                    <?php  if(isset($_SESSION["TypeError"]) && $_SESSION["TypeError"]!="")
                    { echo $_SESSION["TypeError"]; $_SESSION["TypeError"]="";}
                    ?>
                    <?php  if(isset($_SESSION["UploadWasOk"]) && $_SESSION["UploadWasOk"]!="")
                    { echo $_SESSION["UploadWasOk"]; $_SESSION["UploadWasOk"]="";}
                    ?>

                </p>
            </div>

        </form>




    </div>



 </div>
</div>




















<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="../js/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="../js/bootstrap.min.js"></script>
</body>
</html>


