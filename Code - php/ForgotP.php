<?php
/**
 * User: masiya
 * 
 * 
 *
 * This is the forgotten password page
 */

include 'Connections.php';
session_start();
?>
<?php require 'Connections.php';
require '../PhpMailer/PHPMailerAutoload.php';
?>

<?php
$mail = new PHPMailer;
$mail->isSMTP(); // Set mailer to use SMTP
$mail->SMTPDebug = 2;                               // Enable verbose debug output  
$mail->Host = 'smtp.gmail.com';  // Specify main and backup SMTP servers
$mail->SMTPAuth = true;                               // Enable SMTP authentication
$mail->Username = 'teachtech0@gmail.com';                 // SMTP username
$mail->Password = 'Admin2016';                           // SMTP password
$mail->SMTPSecure = 'ssl';                            // Enable TLS encryption, `ssl` also accepted
$mail->Port = 465;

?>



<?php
if(isset($_POST['SendEmail'])){

    $Email=mysqli_real_escape_string($con,$_POST['Email']);


    if($Email==="Email"){
        $_SESSION["InPutMissing"]="Please enter all the details.";
        $_SESSION["InvalidUser"]="";
    }

    else {
        $sql=("select * from user where Email='$Email'");

        $result = mysqli_query($con, $sql);

        /* determine number of rows result set */
        $row_cnt = mysqli_num_rows($result);

        if($row_cnt>0){//User exists

            $row = mysqli_fetch_array($result, MYSQLI_BOTH);

            $randomPassword=rand(78888,98978);//get a random password
            $newPassword=$randomPassword;

            $emailPassword=$newPassword;// copy of new password
            $newPassword=password_hash($newPassword,PASSWORD_BCRYPT,array('cost'=>10));//incrypt the password
            //=md5($newPassword);//encrypt password
            echo $newPassword;
            //update database
            $sql1=("update user set Password='$newPassword' where Email='$Email'");
            mysqli_query($con,$sql1);


            //send new password to the user
            $Subject="Login details";
            $Message="Your password has been changed to".$emailPassword."<br><br>Please login using this password and change it";

            $mail->setFrom('noreply@gmail.com',"noreply");

            $mail->addAddress('teachtech0@gmail.com', 'Admin');     // Add a recipient

            $mail->Subject = $Subject;
            $mail->Body    = $Message;
            $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';


            if(!$mail->send()) {
                echo 'Message could not be sent.';
                echo 'Mailer Error: ' . $mail->ErrorInfo;
            } else {
                $_SESSION["EmailSent"]="Your new password has been sent to your email.";
                $_SESSION["LoginFail"]="";
                $_SESSION["InPutMissing"]="";
                $_SESSION["InvalidUser"]="";
            }



            //mail($Email,$Subject,$Message,$from);

        }

        //number of rows returned is zero
        else{
            $_SESSION["InvalidUser"]="Your account does not exit! Please sign up first.";
            $_SESSION["LoginFail"]="";
            $_SESSION["InPutMissing"]="";
            $_SESSION["EmailSent"]="";

        }
    }


}



?>

<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/html">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>TechGurus</title>

    <!-- Bootstrap -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">
    <link href="../css/custom.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="../js/html5shiv.min.js"></script>
    <script src="../js/respond.min.js"></script>
    <![endif]-->
</head>

<style>
    body{
        padding-top: 40px;
    }
</style>

<body data-spy="scroll" data-target="#my-navbar">

</header>

<!--Navbar-->
   <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation" id="my-navbar">
      <div class="container">
       <div class="navbar-header">
            <a href="Home.php" ><img src="../Images/logo.png" id="logo" class="img-responsive"></a>
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#list-to-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
      </div>
          <div class="collapse navbar-collapse" id="list-to-collapse">

       <ul class="nav navbar-nav">
           <li><li>
           <li><a id="link1" style="color:white;" href="Home.php#Computers" onmouseover="changeColor(this,'#9d9d9d')" onmouseout="changeColor(this,'white')">Computers</a><li>
<li><a id="link2" style="color:white"  href="Home.php#CableAndAdapters" onmouseover="changeColor(this,'#9d9d9d')" onmouseout="changeColor(this,'white')">Adapters & Cables</a><li>
           <li><a id="link3"style="color:white" href="Home.php#Games" onmouseover="changeColor(this,'#9d9d9d')" onmouseout="changeColor(this,'white')">Gaming</a><li>
           <li><a id="link4"style="color:white" href="Home.php#Networking" onmouseover="changeColor(this,'#9d9d9d')" onmouseout="changeColor(this,'white')">Networking</a><li>
           <li><a id="link5" style="color:white;" href="Home.php#Softwares" onmouseover="changeColor(this,'#9d9d9d')" onmouseout="changeColor(this,'white')">Software</a><li>
           <li><a id="link6" style="color:white"  href="Home.php#Speakers" onmouseover="changeColor(this,'#9d9d9d')" onmouseout="changeColor(this,'white')">Speakers</a><li>
           <li><a id="link7"style="color:white" href="Home.php#Storages" onmouseover="changeColor(this,'#9d9d9d')" onmouseout="changeColor(this,'white')">Storage</a><li>
           <li><a id="link8"style="color:white" href="" onmouseover="changeColor(this,'#9d9d9d')" onmouseout="changeColor(this,'white')">Support</a><li>


               <div class="dropdown">
                   <button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown"> Account <span class="caret"></span>
                   </button>

                   <ul class="dropdown-menu">
                       <li><a id="link9"style="color:white" href= "<?php if(isset($_SESSION['UserStatus'])){} else { echo "login.php";}?>"
                              type="button" class="btn" <?php if(isset($_SESSION['UserStatus'])){ echo "disabled";}?>>Login <?php if(isset($_SESSION['UserStatus'])){ echo " (".$_SESSION['UserStatus'].")";}
                               else { echo " (not logged in )";}?></a><li>

                       <li><a id="link9"style="color:white" href= "<?php if(!isset($_SESSION['UserStatus'])){} else { echo "UpdateA.php";}?>"
                              type="button" class="btn" <?php if(!isset($_SESSION['UserStatus'])){ echo "disabled";}?>>Edit Profile</a><li>

                       <li><a id="link9"style="color:white" href= "<?php if(!isset($_SESSION['UserStatus'])){} else { echo "logout.php";}?>"
                              type="button" class="btn" <?php if(!isset($_SESSION['UserStatus'])){ echo "disabled";}?>>Logout</a><li>
                   </ul>

               </div>
           </li>


       </ul>

      </div>
      </div>

   </nav>            <!--Navbar end-->
                <!--Header-->
    <div class="jumbotron text-center">
        <div class="container">
            <h1>TechGurus</h1>
             <h3> We assemble, retail and wholesale Information Technology products and services.</h3>
        </div>
    </div>
<!--header end-->
   </header>


<div class="container">

<div class="row">
    <div class="col-md-6">
        <form role="form" action="" method="POST" name="LoginForm" id="LoginForm">


            <div class="FormElement" >
                <p style="font: italic bold 12px/30px Georgia, serif;color:red;">

                    <?php  if(isset($_SESSION["InvalidUser"]) && $_SESSION["InvalidUser"]!="" )
                    { echo $_SESSION["InvalidUser"];  $_SESSION["InvalidUser"]="";}
                    ?>
                    <?php  if(isset($_SESSION["InPutMissing"]) && $_SESSION["InPutMissing"]!="" )
                    { echo $_SESSION["InPutMissing"];  $_SESSION["InPutMissing"]="";}
                    ?>
                    <?php  if(isset($_SESSION["EmailSent"]) && $_SESSION["EmailSent"]!="" )
                    { echo $_SESSION["EmailSent"];  $_SESSION["EmailSent"]="";}
                    ?>



                </p>
            </div>


            <table class="table table-bordered">

                <tbody>
                <tr>
                    <th>Forgot your password? Don't worry, we will email it to you</th>
                </tr>
                <tr><td>
                        <input type="text" name="Email"  class="form-control" id="Email" placeholder=" Enter email here"  required/>

                    </td></tr>

                </tbody>
                </table>

                <input type="submit" name="SendEmail"  class="btn btn-primary" id="SendEmail" value="Recovery Password">

        </form>
    </div>


    </div>



 </div>





















<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="../js/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="../js/bootstrap.min.js"></script>
</body>
</html>


